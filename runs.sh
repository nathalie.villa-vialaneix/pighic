#! /bin/bash
#
# This file is part of the PigHiC scripts 
# <https://forgemia.inra.fr/nathalie.villa-vialaneix/pighic>
# Copyright (c) 2021 Sylvain Foissac Nathalie Vialaneix
#  
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by  the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <http://www.gnu.org/licenses/>.

# create directory to store results
mkdir results

# 1. Visualization ############################################################
c=1  ; from=165000000   ;  to=200000000    ; d=85   ; t=3
Rscript visualization/vizHiC.R -f "data/chr"$c".Rep1-110-160307.40000.raw.tab,data/chr"$c".Rep1-90-160216.40000.raw.tab,data/chr"$c".Rep2-110-160308.40000.raw.tab,data/chr"$c".Rep2-90-160223.40000.raw.tab,data/chr"$c".Rep3-110-160517.40000.raw.tab,data/chr"$c".Rep3-90-160308.40000.raw.tab" -i "data/index.40000.chr"$c".bed" -c "$c" -s $from -e $to -o "results/matrices.chr"$c"_"$from"-"$to"_d$d" -d $d -n "1,1,2,2,3,3" -a "110,90,110,90,110,90" -t $t

c=13  ; from=136000000   ;  to=161000000    ; d=65   ; t=3
Rscript visualization/vizHiC.R -f "data/chr"$c".Rep1-110-160307.40000.raw.tab,data/chr"$c".Rep1-90-160216.40000.raw.tab,data/chr"$c".Rep2-110-160308.40000.raw.tab,data/chr"$c".Rep2-90-160223.40000.raw.tab,data/chr"$c".Rep3-110-160517.40000.raw.tab,data/chr"$c".Rep3-90-160308.40000.raw.tab" -i "data/index.40000.chr"$c".bed" -c "$c" -s $from -e $to -o "results/matrices.chr"$c"_"$from"-"$to"_d$d" -d $d -n "1,1,2,2,3,3" -a "110,90,110,90,110,90" -t $t

# 2. TADs and A/B compartment analysis ########################################
# Computing number of valid interactions
Rscript tadscomps/nbvalidpairs.R -m "data/Rep1-110-160307_500000.longest18chr.tab,data/Rep1-90-160216_500000.longest18chr.tab,data/Rep2-110-160308_500000.longest18chr.tab,data/Rep2-90-160223_500000.longest18chr.tab,data/Rep3-110-160517_500000.longest18chr.tab,data/Rep3-90-160308_500000.longest18chr.tab" -i "data/index.500000.longest18chr.abs.bed" -r "results"

# Analysis of TADs
# Compute number of TADs
for sample in 50000.Rep1-90-160216 50000.Rep2-90-160223 50000.Rep3-90-160308 merged90.50000 50000.Rep1-110-160307 50000.Rep2-110-160308 50000.Rep3-110-160517 merged110.50000 ; do 
 s=$sample
 tads=`cat data/tads/tads.juicer.$sample.bed | wc -l`
 echo $sample $tads
done  | sort -nrk3 | column -t > results/nboftads
# Plot
Rscript tadscomps/tad_analysis.R -r "results"

# Analysis of A/B compartments
# Compute number of compartments per chromosome
(printf "sample"
for c in {1..18} ; do
 printf "\t"$c
done
echo ""
for s in Rep1-90-160216 Rep2-90-160223 Rep3-90-160308 merged90.500000 Rep1-110-160307 Rep2-110-160308 Rep3-110-160517 merged110.500000 ; do 
 printf $s
 for c in {1..18} ; do
  n=`cat data/ABcomps/compartments.$s.bed | awk -v c=$c '$1==c{print $1,$4}' | uniq | wc -l`
  printf "\t"$n
 done
 echo ""
done) | tee results/compartments.stats.numberperchrom.tsv
# Compute total number of compartments
for sample in Rep1-90-160216 Rep2-90-160223 Rep3-90-160308 merged90.500000 Rep1-110-160307 Rep2-110-160308 Rep3-110-160517 merged110.500000 ; do 
 comps=`cat data/ABcomps/compartments.$sample.bed | wc -l`
 echo $sample $comps
done  | sed 's/\-16[0-9]*//'  | tee results/nbofcompartments
# Test and plot
Rscript tadscomps/ABcomp_analysis.R -r "results"

# 3. Differential analysis ####################################################
# Filtering based on merged matrix (minsum 30)
res=500000
mincount=30
mat1=data/merged.$res.longest18chr.tab
mat2=${mat1%tab}mincount$mincount.tab
cat $mat1 | awk -v mincount=$mincount '$3>=mincount' > $mat2

# load individual matrices and merge to create a common edgeR object
## require approximately 2Go of RAM
Rscript diffanalysis/matrixreader.R -f  "../data/Rep1-110-160307_$res.longest18chr.tab,../data/Rep1-90-160216_$res.longest18chr.tab,../data/Rep2-110-160308_$res.longest18chr.tab,../data/Rep2-90-160223_$res.longest18chr.tab,../data/Rep3-110-160517_$res.longest18chr.tab,../data/Rep3-90-160308_$res.longest18chr.tab" -b "../data/index.$res.longest18chr.abs.bed" -m "../data/merged.$res.longest18chr.mincount$mincount.tab" -c "110,90,110,90,110,90" -n "Rep1-110-160307,Rep1-90-160216,Rep2-110-160308,Rep2-90-160223,Rep3-110-160517,Rep3-90-160308" -o "../results"

# perform differential analysis
## require approximately 10Go of RAM
Rscript diffanalysis/diffanalHiC.R -w "../results"

# 4. BODI analysis  ###########################################################
Rscript bodi/bodi.R -i results/diffbins.txt -b 500000 -m 6 -o results/bodis.txt -p results/bodis.png

# 5. Telomere analysis ########################################################
Rscript telomeres/telomereAnalysis.R -f "data/telomere_contacts.tsv"
