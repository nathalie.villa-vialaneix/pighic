# This file is part of the PigHiC scripts 
# <https://forgemia.inra.fr/nathalie.villa-vialaneix/pighic>
# Copyright (c) 2021 Nathalie Vialaneix, Matthias Zytnicki
#  
# This program is free software: you can redistribute it and/or modify it under 
# the terms of the GNU General Public License as published by  the Free Software
# Foundation, version 3.
# 
# This program is distributed in the hope that it will be useful, but WITHOUT 
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS 
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along with 
# this program. If not, see <http://www.gnu.org/licenses/>.

library("optparse")

##### Read and process options
option_list <- list(
  make_option(c("-i", "--input"), type = "character",
              help = "differential bins in a space delimitated file"),
  make_option(c("-b", "--bin"), type = "numeric",
              help = "bin size"),
  make_option(c("-m", "--minSize"), type = "numeric",
              help = "minimum BODI size (in bins)"),
  make_option(c("-o", "--output"), type = "character",
              help = "output file of the BODIs"),
  make_option(c("-p", "--plot"), help = "output plot of the BODI sizes") 
)

opt_parser <- OptionParser(usage = "Discovery of BODIs", 
                           option_list = option_list)
opt <- parse_args(opt_parser)

##### packages
library("dplyr")
library("tidyr")
library("ggplot2")

##### initialization -----------------------------------------------------------
# Read input file
diffInteraction <- read.delim(opt$input, header = TRUE, sep = " ",
                              colClasses = c("factor", "factor", "integer",
								                             "integer", "real", "real", "real",
                                             "real", "real", "real", "real",
			                                       "real"))

# Make data symmetric
diffInteractionSym <- diffInteraction %>%
  dplyr::filter(chr1 == chr2) %>%
  dplyr::rename(tmp = chr2, chr2 = chr1, chr1 = chr2) %>%
  dplyr::rename(tmp = bin2, bin2 = bin1, bin1 = bin2) %>%
  dplyr::bind_rows(diffInteraction) %>%
  dplyr::select(colnames(diffInteraction)) %>%
  dplyr::arrange(chr1, bin1, chr2, bin2)

# Categorize interactions into (-1, +1)
diffInteractionSimple <- diffInteractionSym %>%
  dplyr::mutate(sign = factor(sign(logFC))) %>%
  dplyr::select(c(chr1, chr2, bin1, bin2, sign))

# Count number of interactions per bin
diffInteractionUni <- diffInteractionSimple %>%
  dplyr::group_by(chr1, bin1, sign) %>%
  dplyr::summarize(n = n()) %>%
  tidyr::spread(key = sign, value = n, fill = 0) %>%
  ungroup()

# When bins are mixes of (-1, +1), it is sometimes possible
#   to classify them, based on majority
classificationFactor <- 10
classify1_1 <- function(table) {
  table %>%
    dplyr::mutate(sign = dplyr::case_when(`-1` >= classificationFactor *  `1`
                                            ~ -1,
                                          `1`  >= classificationFactor * `-1`
                                            ~ 1,
                                          TRUE
                                            ~ 0)) %>%
    dplyr::mutate(sign = factor(sign)) %>%
    dplyr::select(-c(`-1`, `1`))
}
diffInteractionSign <- diffInteractionUni %>% classify1_1()

# Group signed bins into intervals
getIntervals <- function(table) {
  table %>%
  dplyr::group_by(chr1) %>%
  dplyr::mutate(signChange = ifelse(dplyr::lag(sign) != sign, 1, 0) %>%
                coalesce(0)) %>%
  dplyr::mutate(intervalId = cumsum(signChange) + 1) %>%
  dplyr::group_by(chr1, intervalId) %>%
  dplyr::mutate(start = dplyr::first(bin1),
              end   = dplyr::last(bin1),
              size  = end - start + 1) %>%
  dplyr::ungroup(chr1, intervalId) %>%
  dplyr::select(chr1, sign, start, end, size) %>%
  dplyr::distinct()
}
diffInteractionInterval <- getIntervals(diffInteractionSign)

# Compute size of BODIs
getIntervalDensity <- function(table) {
  table %>%
  group_by(sign, size) %>%
  summarise(nIntervals = n()) %>%
  ungroup()
}
diffInteractionIntervalDensity <- getIntervalDensity(diffInteractionInterval)

# Compare with a random distribution
processTable <- function(table) {
  table %>%
  dplyr::mutate(sign = factor(sign(logFC))) %>%
  dplyr::select(c(chr1, chr2, bin1, bin2, sign)) %>%
  dplyr::group_by(chr1, bin1, sign) %>%
  dplyr::summarize(n = n()) %>%
  tidyr::spread(key = sign, value = n, fill = 0) %>%
  dplyr::ungroup() %>%
  classify1_1()
}

n <- 100
..randomizeIntervals <- function(inputTable = inputTable) {
  inputTable %>%
    dplyr::mutate(logFC = sample(logFC)) %>%
    processTable() %>%
    getIntervals() %>%
    getIntervalDensity()
}
.randomizeIntervals <- function(inputTable, n) {
  table <- tibble()
  for (i in seq(n)) {
    table <- ..randomizeIntervals(inputTable) %>%
      dplyr::bind_rows(table)
  }
  return(table)
}
randomizeIntervals <- function(inputTable, n) {
  table <- .randomizeIntervals(inputTable, n)
  table %>%
    dplyr::group_by(sign, size) %>%
    dplyr::summarise(meanIntervals = mean(nIntervals),
                     sdIntervals  = sd(nIntervals)) %>%
    dplyr::ungroup() %>%
    dplyr::rename(nIntervals = meanIntervals)
}
diffInteractionIntervalRandomMean <- randomizeIntervals(diffInteractionSym, n)

# Create the figure
mylabels <- c("-1" = "mostly negative bins",
		      "0" = "neutral",
			  "1" = "mostly positive bins")
p <- bind_rows(diffInteractionIntervalDensity %>%
                 dplyr::mutate(sdIntervals = 0) %>%
                 dplyr::mutate(type = "Observed"),
               diffInteractionIntervalRandomMean %>%
                 dplyr::mutate(type = "Random")) %>%
  dplyr::mutate(size = size * opt$bin) %>%
  ggplot(aes(x = size, y = nIntervals, color = type)) +
    geom_line() +
    geom_errorbar(aes(ymin=nIntervals-sdIntervals,
                      ymax=nIntervals+sdIntervals),
                  width=.2) +
    scale_colour_manual(values = c("orange", "black")) +
    xlim(c(1 * opt$bin, 15 * opt$bin)) +
    scale_y_log10() +
    facet_grid(sign ~ .,
               labeller = labeller(sign = mylabels)) +
    theme_bw() +
    ylab("number of intervals") +
    xlab("interval size")
ggsave(filename = opt$plot, p, width = 7, height = 3, dpi = 300)

# Write BODIs to file
outputBodis <- diffInteractionInterval %>%
  dplyr::filter(size >= opt$minSize) %>%
  dplyr::mutate(start = start * opt$bin) %>%
  dplyr::mutate(end = end * opt$bin) %>%
  dplyr::select(chr1, start, end, size)
options(scipen=10)
write.table(outputBodis, file = opt$output, quote = FALSE, row.names = FALSE,
		    col.names = FALSE)

# session information #####
# script last updated and tested with:
# 
# R version 3.3.3 (2017-03-06)
# Platform: x86_64-pc-linux-gnu (64-bit)
# Running under: CentOS Linux 7 (Core)

# locale:
# [1] LC_CTYPE=en_US.UTF-8       LC_NUMERIC=C              
# [3] LC_TIME=en_US.UTF-8        LC_COLLATE=en_US.UTF-8    
# [5] LC_MONETARY=en_US.UTF-8    LC_MESSAGES=en_US.UTF-8   
# [7] LC_PAPER=en_US.UTF-8       LC_NAME=C                 
# [9] LC_ADDRESS=C               LC_TELEPHONE=C            
# [11] LC_MEASUREMENT=en_US.UTF-8 LC_IDENTIFICATION=C       

# attached base packages:
# [1] stats     graphics  grDevices utils     datasets  base     

# other attached packages:
# [1] bindrcpp_0.2   ggplot2_2.2.1  tidyr_0.8.0    dplyr_0.7.4    optparse_1.4.4

# loaded via a namespace (and not attached):
# [1] Rcpp_0.12.15     bindr_0.1        magrittr_1.5     tidyselect_0.2.4
# [5] munsell_0.4.3    getopt_1.20.2    colorspace_1.3-2 R6_2.2.2        
# [9] rlang_0.2.0      stringr_1.3.0    plyr_1.8.4       tools_3.3.3     
# [13] grid_3.3.3       gtable_0.2.0     utf8_1.1.3       cli_1.0.0       
# [17] digest_0.6.15    lazyeval_0.2.1   assertthat_0.2.0 tibble_1.4.2    
# [21] crayon_1.3.4     purrr_0.2.4      reshape2_1.4.3   glue_1.2.0      
# [25] labeling_0.3     stringi_1.1.6    pillar_1.2.1     methods_3.3.3   
# [29] scales_0.5.0     pkgconfig_2.0.1 
